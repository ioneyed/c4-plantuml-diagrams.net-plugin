let PLUGIN_VERSION="0.0.1"
/* C4 Person Shape */
function C4Person(bounds, fill, stroke, strokewidth) {
	this.bounds = bounds;
	this.fill = fill;
	this.stroke = stroke;
	this.strokewidth = (strokewidth != null) ? strokewidth : 1;

}
mxUtils.extend(C4Person, mxShape);
C4Person.prototype.cst = { START: 'C4puml.person' }
C4Person.prototype.paintVertexShape = function (c, x, y, w, h) {
	c.translate(x, y);
	var headSize = Math.min(w / 2, h / 3);
	var r = headSize / 2;
	c.ellipse(w * 0.5 - headSize * 0.5, 0, headSize, headSize);
	c.fillAndStroke();
	c.begin();
	c.moveTo(0, headSize * 0.8 + r);
	c.arcTo(r, r, 0, 0, 1, r, headSize * 0.8);
	c.lineTo(w - r, headSize * 0.8);
	c.arcTo(r, r, 0, 0, 1, w, headSize * 0.8 + r);
	c.lineTo(w, h - r);
	c.arcTo(r, r, 0, 0, 1, w - r, h);
	c.lineTo(r, h);
	c.arcTo(r, r, 0, 0, 1, 0, h - r);
	c.close();
	c.fillAndStroke();
	c.setShadow(false);
	c.ellipse(w * 0.5 - headSize * 0.5, 0, headSize, headSize);
	c.fillAndStroke();
}
C4Person.prototype.getLabelMargins = function (rect) {
	var headSize = Math.min(rect.width / 2, rect.height / 3);
	return new mxRectangle(0, headSize * .8, 0, 0);
}

mxCellRenderer.registerShape(C4Person.prototype.cst.START, C4Person);

function C4Palette() {
	this.Shapes = {
		"Core":{
			"Person":{
				"title":"C4 PUML Person",
				"entry": this.Person().getEntry()
			},
			"SoftwareSystem":{
				"title":"C4 PUML Software System",
				"entry": this.SoftwareSystem().getEntry()
			},
			"Container":{
				"title":"C4 PUML Container",
				"entry": this.Container().getEntry()
			},
			"ContainerDb":{
				"title":"C4 PUML Container Database",
				"entry": this.ContainerDb().getEntry()
			},
			"Component":{
				"title":"C4 PUML Component",
				"entry": this.Component().getEntry()
			},
			"Boundary":{
				"title":"C4 PUML Boundary",
				"entry": this.Boundary().getEntry()
			},
			"Relationship":{
				"title":"C4 PUML Relationship",
				"entry": this.Relationship().getEntry()
			}
		},
		"Context":{
			"Person":{
				"title":"C4 PUML Person",
				"entry": this.Person().getEntry()
			},
			"ExtPerson":{
				"title":"C4 PUML External Person",
				"entry": this.ExtPerson().getEntry()
			},
			"SoftwareSystem":{
				"title":"C4 PUML Software System",
				"entry": this.SoftwareSystem().getEntry()
			},
			"SoftwareSystemDb":{
				"title":"C4 PUML Software System DB",
				"entry": this.SoftwareSystemDb().getEntry()
			},
			"ExtSoftwareSystemDb":{
				"title":"C4 PUML External Software System DB",
				"entry": this.ExtSoftwareSystem().getEntry()
			},
			"Boundary":{
				"title":"C4 PUML Boundary",
				"entry": this.Boundary().getEntry()
			},
			"EnterpriseBoundary":{
				"title":"C4 PUML Enterprise Boundary",
				"entry": this.EnterpriseBoundary().getEntry()
			},
			"SystemBoundary":{
				"title":"C4 PUML System Boundary",
				"entry": this.SystemBoundary().getEntry()
			},
			"Relationship":{
				"title":"C4 PUML Relationship",
				"entry": this.Relationship().getEntry()
			},
			"AsyncRelationship":{
				"title":"C4 PUML Async Relationship",
				"entry": this.AsyncRelationship().getEntry()
			}
		},
		"Container":{
			"Container":{
				"title":"C4 PUML Container",
				"entry": this.Container().getEntry()
			},
			"ContainerDb":{
				"title":"C4 PUML Container Database",
				"entry": this.ContainerDb().getEntry()
			},
			"Boundary":{
				"title":"C4 PUML Boundary",
				"entry": this.Boundary().getEntry()
			},
			"ContainerBoundary":{
				"title":"C4 PUML Container Boundary",
				"entry": this.ContainerBoundary().getEntry()
			},
			"Relationship":{
				"title":"C4 PUML Relationship",
				"entry": this.Relationship().getEntry()
			},
			"AsyncRelationship":{
				"title":"C4 PUML Async Relationship",
				"entry": this.AsyncRelationship().getEntry()
			}
		},
		"Component":{},
	}
}
/** C4 Palette Shapes
 * These are the core shapes for the C4Model of Architecture Diagramming (https://c4model.com)
 * Core Shapes - Person, Software System, Container, Component Relationship
 * return new C4PaletteItem
 */
C4Palette.prototype.Person = () => {
	return new C4PaletteItem(
		"Person",
		1.1,
		1.4,
		{ "shape": "C4puml.person", "points": "[[0.5,0,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0]]" },
		{
			"placeholders": "1",
			"C4Alias": "alias",
			"C4Name": "name",
			"C4Puml": "Person",
			"C4Type": "Person",
			"C4Description": "Description",
			"label": `<div><b>%C4Name%</b></div><div style="font-size: x-small">[%C4Type%]</div><br /><div>%C4Description%</div>`
		})
}
C4Palette.prototype.ExtPerson = () => {
	return new C4PaletteItem(
		"External Person",
		1.1,
		1.4,
		{ 
			"fillColor":"#686868",
			"strokeColor":"#8A8A8A",
			"shape": "C4puml.person", 
			"points": "[[0.5,0,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0]]" },
		{
			"placeholders": "1",
			"C4Alias": "alias",
			"C4Name": "name",
			"C4Puml": "PersonExt",
			"C4Type": "External Person",
			"C4Description": "Description",
			"label": `<div><b>%C4Name%</b></div><div style="font-size: x-small">[%C4Type%]</div><br /><div>%C4Description%</div>`
		})
}
C4Palette.prototype.SoftwareSystem = () => {
	return new C4PaletteItem(
		"System",
		1.6,
		1.1,
		{
			"fillColor":"#1168BD",
			"strokeColor":"#1168BD"
		},
		{
			"label": `<div><b>%C4Name%</b></div><div style="font-size: x-small">[%C4Type%]</div><br /><br /><div>%C4Description%</div>`
		})
}
C4Palette.prototype.ExtSoftwareSystem = () => {
	return new C4PaletteItem(
		"System",
		1.6,
		1.1,
		{
			"fillColor":"#999999",
			"strokeColor":"#8A8A8A"
		},
		{
			"C4Type":"External System",
			"C4Puml":"System_Ext",
			"label": `<div><b>%C4Name%</b></div><div style="font-size: x-small">[%C4Type%]</div><br /><br /><div>%C4Description%</div>`
		})
}
C4Palette.prototype.SoftwareSystemDb = () => {
	return new C4PaletteItem(
		"System Database",
		1.6,
		1.1,
		{
			"shape":"cylinder",
			"boundedLbl":"1",
			"fillColor":"#1168BD",
			"strokeColor":"#3C7FC0",
			"backgroundColor":"none",
			"points":"[[0.5,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.5,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]]"
		},
		{
			"C4Type":"System Database",
			"C4Technology":"Technology",
			"C4Puml":"SystemDb",
			"label":`<b>%C4Name%</b><div style="font-size: x-small">[Container: %C4Technology%]</div><br><div>%C4Description%</div>`
		})
	
}
C4Palette.prototype.ExtSystemDb = () => {
	return new C4PaletteItem(
		"External System Database",
		1.6,
		1.1,
		{
			"shape":"cylinder",
			"boundedLbl":"1",
			"fillColor":"#999999",
			"strokeColor":"#8A8A8A",
			"backgroundColor":"none",
			"points":"[[0.5,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.5,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]]"
		},
		{
			"C4Type":"External System Database",
			"C4Technology":"Technology",
			"C4Puml":"SystemDb_Ext",
			"label":`<b>%C4Name%</b><div style="font-size: x-small">[Container: %C4Technology%]</div><br><div>%C4Description%</div>`
		})
	
}
C4Palette.prototype.Container = () => {
	return new C4PaletteItem(
		"Container",
		1.6,
		1.1,
		{
			"fillColor":"#438DD5",
			"strokeColor":"#3C7FC0",
			"backgroundColor":"none"
		},
		{
			"C4Type":"Container",
			"C4Technology":"Technology",
			"C4Puml":"Container",
			"label":`<b>%C4Name%</b><div style="font-size: x-small">[Container: %C4Technology%]</div><br><div>%C4Description%</div>`
		})
	
}
C4Palette.prototype.ContainerDb = () => {
	return new C4PaletteItem(
		"Container Database",
		1.6,
		1.1,
		{
			"shape":"cylinder",
			"boundedLbl":"1",
			"fillColor":"#438DD5",
			"strokeColor":"#3C7FC0",
			"backgroundColor":"none",
			"points":"[[0.5,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.5,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]]"
		},
		{
			"C4Type":"Container",
			"C4Technology":"Technology",
			"C4Puml":"ContainerDb",
			"label":`<b>%C4Name%</b><div style="font-size: x-small">[Container: %C4Technology%]</div><br><div>%C4Description%</div>`
		})
	
}
C4Palette.prototype.Component = () => {
	return new C4PaletteItem(
		"Component",
		1.6,
		1.1,
		{
			"fillColor":"#85BBF0",
			"strokeColor":"#78A8D8",
			"backgroundColor":"none"
		},
		{
			"C4Type":"Component",
			"C4Technology":"Technology",
			"C4Puml":"Component",
			"label":`<b>%C4Name%</b><div style="font-size: x-small">[Component: %C4Technology%]</div><br><div>%C4Description%</div>`
		})
}
C4Palette.prototype.Relationship = () => {
	return new C4PaletteItem(
		"Relationship",
		1.6,
		1.1,
		{},
		{},
		true
	)
}
C4Palette.prototype.AsyncRelationship = () => {
	return new C4PaletteItem(
		"AsyncRelationship",
		1.6,
		1.1,
		{"dashed":"1"},
		{
			"C4Type":"Async Relationship",
			"label":`<div style="text-align: left"><div style="text-align: center"><b>%C4Description%</b></div><div style="text-align: center">[Async:%C4Technology%]</div></div>`
		},
		true
	)
}
C4Palette.prototype.Boundary = () => {
	return new C4PaletteItem(
		"Boundary",
		2,
		1.7,
		{
			"align":"left",
			"verticalAlign":"bottom",
			"arcSize":"5",
			"dashed":"1",
			"fillColor":"none",
			"fontColor":"#000",
			"container":"1",
			"collapsible":"1",
			"autosize":"1",
			"metaData": `{"C4Type":{"editable":true},"C4Puml":{"editable":false}}`,
			"recursiveResize": "0"
		},

		{
			"C4Type":"Boundary",
			"C4Puml":"Boundary",
			"label":`<div style="text-align: left">%C4Name%</div><div style="text-align: left">[%C4Type%]</div>`
		}
	)
}
C4Palette.prototype.ContainerBoundary = () => {
	return new C4PaletteItem(
		"Container Boundary",
		2,
		1.7,
		{
			"align":"left",
			"verticalAlign":"bottom",
			"arcSize":"5",
			"dashed":"1",
			"fillColor":"none",
			"fontColor":"#000",
			"container":"1",
			"collapsible":"1",
			"autosize":"1",
			"metaData": `{"C4Type":{"editable":false},"C4Puml":{"editable":false}}`,
			"recursiveResize": "0"
		},
		{
			"C4Type":"Container",
			"C4Puml":"Container_Boundary",
			"label":`<div style="text-align: left">%C4Name%</div><div style="text-align: left">[%C4Type%]</div>`
		}
	)
}
C4Palette.prototype.EnterpriseBoundary = () => {
	return new C4PaletteItem(
		"Enterprise Boundary",
		2,
		1.7,
		{
			"align":"left",
			"verticalAlign":"bottom",
			"arcSize":"5",
			"dashed":"1",
			"fillColor":"none",
			"fontColor":"#000",
			"container":"1",
			"collapsible":"1",
			"autosize":"1",
			"metaData": `{"C4Type":{"editable":false},"C4Puml":{"editable":false}}`,
			"recursiveResize": "0"
		},
		{
			"C4Type":"Enterprise",
			"C4Puml":"Enterprise_Boundary",
			"label":`<div style="text-align: left">%C4Name%</div><div style="text-align: left">[%C4Type%]</div>`
		}
	)
}
C4Palette.prototype.SystemBoundary = () => {
	return new C4PaletteItem(
		"System Boundary",
		2,
		1.7,
		{
			"align":"left",
			"verticalAlign":"bottom",
			"arcSize":"5",
			"dashed":"1",
			"fillColor":"none",
			"fontColor":"#000",
			"container":"1",
			"collapsible":"1",
			"autosize":"1",
			"metaData": `{"C4Type":{"editable":false},"C4Puml":{"editable":false}}`,
			"recursiveResize": "0"
		},
		{
			"C4Type":"System Boundary",
			"C4Puml":"System_Boundary",
			"label":`<div style="text-align: left">%C4Name%</div><div style="text-align: left">[%C4Type%]</div>`
		}
	)
}

C4Palette.prototype.register = function(ui){
	let palette = this
	Sidebar.prototype.addC4PlantUMLPalette = function () {
		let entries = {}
		let _sbthis = this
		Object.keys(palette.Shapes).forEach((category)=>{
			entries[category]=[]
			Object.keys(palette.Shapes[category]).forEach((shape)=>{
				entries[category].push(_sbthis.addEntry(palette.Shapes[category][shape].title, palette.Shapes[category][shape].entry))
			})
			if(Object.keys(entries[category]).length>0){
				_sbthis.addPaletteFunctions("C4PUML", `C4 - ${category}`, false, entries[category])
			}
		})

	}
	ui.sidebar.addC4PlantUMLPalette();
}

/** C4 Palette Extended Shapes 
 * These are manipulations to align with C4Puml by RicardoNiepel's C4 PlantUML Work
 * Extended Shapes - 
 * 		Database, 
 * 		External Person, External System, External Database
 *      Boundary, Container Boundary, Enterprise Boundary, System Boundary
 *      Component
*/


/* C4PaletteItem generates the shape itself with the C4 Attributes based on metadata passed */
function C4PaletteItem(label, widthMultiplier, heightMultiplier, settings, attributes, edge = false) {
	this.DEFAULT_SHAPE_SETTINGS = {
		"html": "1",
		"dashed": "0",
		"whiteSpace": "wrap",
		"fillColor": "#08427B",
		"strokeColor": "#073B6F",
		"fontColor": "#ffffff",
		"labelBackgroundColor":"none",
		"rounded": "1",
		"arcSize":"5",
		"align": "center",
		"metaEdit": "1",
		"points": "[[0.25,0,0],[0.5,0,0],[0.75,0,0],[1,0.25,0],[1,0.5,0],[1,0.75,0],[0.75,1,0],[0.5,1,0],[0.25,1,0],[0,0.75,0],[0,0.5,0],[0,0.25,0]]",
		"metaData": `{"C4Type":{"editable":false},"C4Puml":{"editable":false}}`,
	}
	this.DEFAULT_SHAPE_ATTRIBUTES = {
		"placeholders": "1",
		"C4Alias": "Alias",
		"C4Description": "Description",
		"C4Name":"Name",
		"C4Puml": "System",
		"C4Type": "System",
	}
	this.DEFAULT_REL_SETTINGS = {
		"edgeStyle":"none",
		"rounded":"0",
		"html":"1",
		"entryX":"0",
		"entryY":"0",
		"jettySize":"auto",
		"orthogonalLoop":"1",
		"strokeColor":"#707070",
		"strokeWidth":"2",
		"fontColor":"#707070",
		"jumpStyle":"arc",
		"metaEdit":"1",
		"metaData":`{"C4Type":{"editable":false},"C4Puml":{"editable":false}}`
	}
	this.DEFAULT_REL_ATTRIBUTES = {
		"placeholders":"1",
		"C4Type":"Relationship",
		"C4Puml":"Rel",
		"C4Description":"Description",
		"C4Technology":"technology",
		"label":`<div style="text-align: left"><div style="text-align: center"><b>%C4Description%</b></div><div style="text-align: center">[%C4Technology%]</div></div>`
	}
	this.edge = edge
	this.label = label
	this.shape = edge ? this.generateRelationship(widthMultiplier, heightMultiplier, settings, attributes) : this.generateShape(widthMultiplier, heightMultiplier, settings, attributes)


}
C4PaletteItem.prototype._merge = (...arguments) => {
	let target = {};
	let merger = (obj) => {
		for (let prop in obj) {
			if (obj.hasOwnProperty(prop)) {
				if (Object.prototype.toString.call(obj[prop]) === '[object Object]') {
					target[prop] = merge(target[prop], obj[prop]);
				} else {
					target[prop] = obj[prop];
				}
			}
		}
	};
	for (let i = 0; i < arguments.length; i++) {
		merger(arguments[i]);
	}
	return target

}
C4PaletteItem.prototype.generateShape = function (widthMultiplier, heightMultiplier, settings, attributes) {
	let instanceSettings = this._merge(this.DEFAULT_SHAPE_SETTINGS, settings)
	let instanceAttributes = this._merge(this.DEFAULT_SHAPE_ATTRIBUTES, attributes)
	let s = []
	Object.keys(instanceSettings).forEach((key) => {
		s.push(`${key}=${instanceSettings[key]}`)
	})
	let shape = new mxCell('', new mxGeometry(0, 0, 100 * widthMultiplier, 100 * heightMultiplier), s.join(';'))
	shape.vertex = true
	shape.setValue(mxUtils.createXmlDocument().createElement('object'))
	Object.keys(instanceAttributes).forEach((attr) => {
		shape.setAttribute(attr, instanceAttributes[attr])
	})
	return shape
}
C4PaletteItem.prototype.generateRelationship = function(widthMultiplier, settings, attributes){
	let instanceSettings = this._merge(this.DEFAULT_REL_SETTINGS, settings)
	let instanceAttributes = this._merge(this.DEFAULT_REL_ATTRIBUTES, attributes)
	let s = []
	Object.keys(instanceSettings).forEach((key) => {
		s.push(`${key}=${instanceSettings[key]}`)
	})
	
	var edge = new mxCell('',new mxGeometry(0, 0, 100 * widthMultiplier, 0), s.join(';'));
	edge.geometry.setTerminalPoint(new mxPoint(0, 0), true);
	edge.geometry.setTerminalPoint(new mxPoint(100 * widthMultiplier, 0), false);
	edge.geometry.relative = true;
	edge.edge = true;
	edge.setValue(mxUtils.createXmlDocument().createElement('object'));
	Object.keys(instanceAttributes).forEach((attr) => {
		edge.setAttribute(attr, instanceAttributes[attr])
	})
	return edge

}
C4PaletteItem.prototype.getEntry = function () {
	return !this.edge ? () => { return sb.createVertexTemplateFromCells([this.shape], this.shape.geometry.width, this.shape.geometry.height, this.label)} 
				: () => { return sb.createEdgeTemplateFromCells([this.shape], this.shape.geometry.width, this.shape.geometry.height, this.label)}
}

Draw.loadPlugin(function (ui) {
	new C4Palette().register(ui)
	
	let capitalize = (s) => {
		if (typeof s !== 'string') return ''
		return s.charAt(0).toUpperCase() + s.slice(1)
	}


	let isC4 = (mxcell) => {
		if (typeof mxcell.value == "object") {
			if (mxcell.value.attributes.length > 0) {
				return (mxcell.value.attributes.C4Type && mxcell.value.attributes.C4Puml && mxcell.value.attributes.C4Description)
			}
		}
		return false
	}
	let getC4 = (mxcell) => isC4(mxcell) ? {
				alias: mxcell.value.attributes.C4Alias ? mxcell.value.attributes.C4Alias.value.replaceAll("\n"," ").replaceAll(" ","_") : "",
				description: mxcell.value.attributes.C4Description ? mxcell.value.attributes.C4Description.value.replaceAll("\n"," ") : "",
				name: mxcell.value.attributes.C4Name ? mxcell.value.attributes.C4Name.value.replaceAll("\n"," ") : "",
				type: capitalize(mxcell.value.attributes.C4Type.value),
				technology: mxcell.value.attributes.C4Technology ? mxcell.value.attributes.C4Technology.value.replaceAll("\n"," ") : "",
				uml: capitalize(mxcell.value.attributes.C4Puml.value)
			} : null
	let isEdge = (mxcell) => mxcell.edge
	let isContainer = (mxcell) => mxcell.children
	let extractShape = (shape) => {
		let C4 = getC4(shape)
		return [`${C4.uml}(${C4.alias},"${C4.name}","${C4.description}")`]
	}
	let extractEdges = (edges) => {
		let relationships = []
		edges.forEach(edge => {
			if(isC4(edge)){
				let rel = getC4(edge)
				let src = isC4(edge.source) ? getC4(edge.source) : null
				let dest = isC4(edge.target) ? getC4(edge.target) : null
				if( rel && src && dest){
					if(rel.technology){
						relationships.push(`${rel.uml}(${src.alias}, ${dest.alias}, "${rel.description}", "${rel.technology}")`)
					}else{
						relationships.push(`${rel.uml}(${src.alias}, ${dest.alias}, "${rel.description}")`)
					}
				}
			}
		})
		return relationships
	}
	let extractBoundary = (ui, boundary) => {
		let container = {
			"rels":[],
			"shapes":[],
		}
		container.rels.push(...extractEdges(ui.editor.graph.getOutgoingEdges(boundary)))
		if(!isEdge(boundary) && isC4(boundary)){
			if(isContainer(boundary)){
				let c4boundary = getC4(boundary)
				container.shapes.push(`${c4boundary.uml}(${c4boundary.alias},${c4boundary.name}){`)
				boundary.children.forEach((bounds)=>{
					container.rels.push(...extractEdges(ui.editor.graph.getOutgoingEdges(bounds)))
					if(!isEdge(bounds) && isC4(bounds)){
						if(isContainer(bounds)){
							let nestedBoundary = extractBoundary(ui, bounds)
							container.shapes.push(...nestedBoundary.shapes)
							container.rels.push(...nestedBoundary.rels)
						}else{
							container.shapes.push(...extractShape(bounds))
						}
					}
				})
				container.shapes.push("}")
			}else{
				container.shapes.push(...extractShape(boundary))
			}
		}
		return container
	}
	// Adds resource for action
	mxResources.parse('extractC4PlantUML=Extract C4 PlantUML...');

	/* Adds the action to Extract the C4 PlantUML (pUML)
	 * This will work from all children of the root from mxcell to its edges
	 * placing all edges (relationships) in their own array to be printed at the bottom.
	 */
	ui.actions.addAction('extractC4PlantUML', function () {
		let root = ui.editor.graph.getDefaultParent();
		let text = [
			"'Created using the C4 Diagrams.net Plugin",
			"'Author: ioneyed <Robert Buchanan>",
			"'Source: https://gitlab.com/ioneyed/c4-plantuml-diagrams.net-plugin",
			`'Version: ${PLUGIN_VERSION}`,
			"@startuml","!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Container.puml\n"];
		let rels = []
		root.children.forEach(rootCell => {
			let boundaries = extractBoundary(ui, rootCell)
			text.push(...boundaries.shapes)
			rels.push(...boundaries.rels)
			
			// let edges = ui.editor.graph.getOutgoingEdges(rootCell)
			// /* This will check that the mxcell is not an Edge (relationship) and is a valid C4 shape.
			//  * The diagram can contain non-C4 shapes, this will exclude them
			//  */
			// if (!isEdge(rootCell) && isC4(rootCell)) {
			// 	if(isContainer(rootCell)){
			// 		//do stuff with containers
			// 	}else{
			// 		text.push(...extractShape(rootCell));

			// 	}
			// }
			// rels.push(...extractEdges(edges))
		});
		text.push(...rels)
		text.push("\n@enduml")
		var dlg = new C4ExtractDialog(ui, text.join('\n'),
			null, null, null, 'Extracted C4 PlantUML:');
		dlg.showPreviewOption = false;
		ui.showDialog(dlg.container, 440, 240, true, true);
		dlg.init();
	});

	var menu = ui.menus.get('extras');
	var oldFunct = menu.funct;

	menu.funct = function (menu, parent) {
		oldFunct.apply(this, arguments);

		ui.menus.addMenuItems(menu, ['-', 'extractC4PlantUML'], parent);
	};
});
/**
 * Constructs a new embed dialog
 */
var C4ExtractDialog = function (editorUi, result, timeout, ignoreSize, previewFn, title, tweet) {
	tweet = (tweet != null) ? tweet : 'Check out the diagram I made using @drawio';
	var div = document.createElement('div');
	var maxSize = 500000;
	var maxFbSize = 51200;
	var maxTwitterSize = 7168;

	// Checks if result is a link
	var validUrl = /^https?:\/\//.test(result) || /^mailto:\/\//.test(result);

	if (title != null) {
		mxUtils.write(div, title);
	}
	else {
		mxUtils.write(div, mxResources.get((result.length < maxSize) ?
			((validUrl) ? 'link' : 'mainEmbedNotice') : 'preview') + ':');
	}
	mxUtils.br(div);

	var size = document.createElement('div');
	size.style.position = 'absolute';
	size.style.top = '30px';
	size.style.right = '30px';
	size.style.color = 'gray';
	mxUtils.write(size, editorUi.formatFileSize(result.length));

	div.appendChild(size);

	// Using DIV for faster rendering
	var text = document.createElement('textarea');
	text.setAttribute('autocomplete', 'off');
	text.setAttribute('autocorrect', 'off');
	text.setAttribute('autocapitalize', 'off');
	text.setAttribute('spellcheck', 'false');
	text.style.fontFamily = 'monospace';
	text.style.wordBreak = 'break-all';
	text.style.marginTop = '10px';
	text.style.resize = 'none';
	text.style.height = '150px';
	text.style.width = '440px';
	text.style.border = '1px solid gray';
	text.value = mxResources.get('updatingDocument');
	div.appendChild(text);
	mxUtils.br(div);

	this.init = function () {
		window.setTimeout(function () {
			if (result.length < maxSize) {
				text.value = result;
				text.focus();

				if (mxClient.IS_GC || mxClient.IS_FF || document.documentMode >= 5 || mxClient.IS_QUIRKS) {
					text.select();
				}
				else {
					document.execCommand('selectAll', false, null);
				}
			}
			else {
				text.setAttribute('readonly', 'true');
				text.value = mxResources.get('tooLargeUseDownload');
			}
		}, 0);
	};

	var buttons = document.createElement('div');
	buttons.style.position = 'absolute';
	buttons.style.bottom = '36px';
	buttons.style.right = '32px';


	if (!validUrl || result.length > 7500) {
		var downloadBtn = mxUtils.button(mxResources.get('download'), function () {
			editorUi.hideDialog();
			editorUi.saveData('embed.wsd', 'txt', result, 'text/plain');
		});

		downloadBtn.className = 'geBtn';
		buttons.appendChild(downloadBtn);
	}

	// Twitter-intent does not allow more characters, must be pasted manually
	if (validUrl && (!editorUi.isOffline() || mxClient.IS_CHROMEAPP)) {
		if (result.length < maxFbSize) {
			var fbBtn = mxUtils.button('', function () {
				try {
					var url = 'https://www.facebook.com/sharer.php?p[url]=' +
						encodeURIComponent(text.value);
					editorUi.openLink(url);
				}
				catch (e) {
					editorUi.handleError({ message: e.message || mxResources.get('drawingTooLarge') });
				}
			});

			var img = document.createElement('img');
			img.setAttribute('src', Editor.facebookImage);
			img.setAttribute('width', '18');
			img.setAttribute('height', '18');
			img.setAttribute('border', '0');

			fbBtn.appendChild(img);
			fbBtn.setAttribute('title', mxResources.get('facebook') + ' (' +
				editorUi.formatFileSize(maxFbSize) + ' max)');
			fbBtn.style.verticalAlign = 'bottom';
			fbBtn.style.paddingTop = '4px';
			fbBtn.style.minWidth = '46px'
			fbBtn.className = 'geBtn';
			buttons.appendChild(fbBtn);
		}

		if (result.length < maxTwitterSize) {
			var tweetBtn = mxUtils.button('', function () {
				try {
					var url = 'https://twitter.com/intent/tweet?text=' +
						encodeURIComponent(tweet) + '&url=' +
						encodeURIComponent(text.value);
					editorUi.openLink(url);
				}
				catch (e) {
					editorUi.handleError({ message: e.message || mxResources.get('drawingTooLarge') });
				}
			});

			var img = document.createElement('img');
			img.setAttribute('src', Editor.tweetImage);
			img.setAttribute('width', '18');
			img.setAttribute('height', '18');
			img.setAttribute('border', '0');
			img.style.marginBottom = '5px'

			tweetBtn.appendChild(img);
			tweetBtn.setAttribute('title', mxResources.get('twitter') + ' (' +
				editorUi.formatFileSize(maxTwitterSize) + ' max)');
			tweetBtn.style.verticalAlign = 'bottom';
			tweetBtn.style.paddingTop = '4px';
			tweetBtn.style.minWidth = '46px'
			tweetBtn.className = 'geBtn';
			buttons.appendChild(tweetBtn);
		}
	}

	var closeBtn = mxUtils.button(mxResources.get('close'), function () {
		editorUi.hideDialog();
	});

	buttons.appendChild(closeBtn);

	var copyBtn = mxUtils.button(mxResources.get('copy'), function () {
		text.focus();

		if (mxClient.IS_GC || mxClient.IS_FF || document.documentMode >= 5 || mxClient.IS_QUIRKS) {
			text.select();
		}
		else {
			document.execCommand('selectAll', false, null);
		}

		document.execCommand('copy');
		editorUi.alert(mxResources.get('copiedToClipboard'));
	});

	if (result.length < maxSize) {
		// Does not work in Safari and shows annoying dialog for IE11-
		if (!mxClient.IS_SF && document.documentMode == null) {
			buttons.appendChild(copyBtn);
			copyBtn.className = 'geBtn gePrimaryBtn';
			closeBtn.className = 'geBtn';
		}
		else {
			closeBtn.className = 'geBtn gePrimaryBtn';
		}
	}
	else {
		buttons.appendChild(previewBtn);
		closeBtn.className = 'geBtn';
		previewBtn.className = 'geBtn gePrimaryBtn';
	}

	div.appendChild(buttons);
	this.container = div;
};
