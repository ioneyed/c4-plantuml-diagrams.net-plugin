# C4 PlantUML Diagrams.net Plugin

C4 PlantUML Plugin for diagrams.net (formerly draw.io). This plugin adds the appropriate Shape Library, 2-way C4 PUML Parser, and diagram linter.

# Installation 

We will be using [Diagrams.net](https://Diagrams.net) (formerly known as - [Draw.io](https://www.diagrams.net/blog/move-diagrams-net)) offline (_read_ Desktop Application) version.

## Obtain Diagrams.net Desktop


### Via the Browser

**DEPRECATED** - due to security reasons this method will no longer work and has no plans on working in the future per the authors of Diagrams.net.

Visit the following url [https://app.diagrams.net](https://app.diagrams.net) in a modern browser (Edge, Firefox, Chrome) and you will find a prompt that will ask you if you would like to install the application. We will be using Chrome for the examples below.

Click the install button in the toolbar

![Install Toolbar Icon](docs/images/chrome_install_toolbar.png)

Then click **Install**

![Click Install](docs/images/chrome_install_app.png)

Launch the Progressive Web Application (usually an icon on your desktop or in your Chrome Apps).


### Via Package Manager

If you have any of the following package managers you can install Diagrams.net via command line

**Homebrew**

`brew install --cask drawio`

**Scoop (Windows)**

This will add the Extras bucket where draw.io exists.

`scoop bucket add extras` 

This will install draw.io after the bucket has been added.

`scoop install draw.io`

**Chocolatey**

`choco install drawio`

**Debian**

```
wget https://github.com/jgraph/drawio-desktop/releases/download/v13.6.2/draw.io-amd64-13.6.2.deb \
  && sudo dpkg -i draw.io-amd64-13.6.2.deb \
  && sudo rm draw.io-amd64-13.6.2.deb
  ```

**Ubuntu (Snap)**

`sudo snap install drawio`

### Via Source Control

Diagrams.net is available at the following [Github Repo](https://github.com/jgraph/drawio-desktop). You can follow instructions to build your own or download from the releases section.


## Enable the Plugin

Click "Create a new Diagram"

![Create a new Diagram](docs/images/diagrams_net_create_open.png)

If you are prompted for a storage mechanism click "Decide Later".

Click on "Extras > Plugins" to be prompted with the following empty plugins box:

![Empty Plugins](docs/images/diagrams_net_extra_plugins_empty.png)

Click "Add"

![Add Plugin](docs/images/diagrams_net_extra_plugins_add.png)

Click "Select File"

*The next step can be either method -  download the file or using the native file system fetching capabilities*

1. If you want to use the native file system fetching capabilities you will need to enter the following URL into the file box

`https://gitlab.com/ioneyed/c4-plantuml-diagrams.net-plugin/-/raw/main/c4.js`

2. If you want to download the file yourself you can use your favorite tool, clone the project, or visit the url above and copy the contents to a file. If you chose this method, browse to the file in the **Select File** explorer window and click *Open/Add*

You will now see the file `c4.js` or some variation in the Plugins list. Click **Apply** and restart the application.

## Verify the Installation

Once the plugin is installed and working you will notice 2 things - 

1. New Shapes 

![New C4 Shapes](docs/images/diagrams_net_new_c4_shapes.png)

2. New Extra's option `Extract C4 PlantUML...`

![New Extras Option](docs/images/diagrams_net_new_extras_option.png)








